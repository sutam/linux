To run this assignment:

   source run_assign6.sh > log

   and inspect the file log

The last couple of lines from the log will show something
like this:

[  615.449181] Module traverse unloaded
[  615.454182] Loading module traverse
[  615.454186] Process Id [ 4701 ], command line [ insmod ]
[  615.454187] Process Id [ 4144 ], command line [ bash ]
[  615.454188] Process Id [ 2883 ], command line [ gnome-terminal- ]
[  615.454189] Process Id [ 1 ], command line [ systemd ]

So se can see that the module TraverseProcess is loaded 

    It starts in the current process <insmod> where init_module 
is called and then its parent process of <bash> shell and then
its parent <terminal> and finally the root process <init> with process
Id 1.

