/*
 *    TraverseProcess.c
 *
 *        Description:  
 *
 *        A kernel module to traverse through all the parents starting from
 *        current process until the init_task.
 *        Print the command line and the PID of every process along the way
 * 
 *        A simple init_module is used for the traversal
 *
 *        Kernel variables and macros used:
 *            init_task     The variable for init process pointer
 *            current       Is a macro which returns pointer to the current task
 */
#include <linux/kernel.h>    /* printk()  */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/sched.h>

int traverse_init(void) {

       struct task_struct *task;

       printk( KERN_INFO "Loading module traverse \n" );

       /* current is a macro which returns the current task */
       /* traverse from current to their parents until the init process (init_task) */
       for ( task = current; task != &init_task; task = task->parent )
       {
             printk( KERN_INFO "Process Id [ %d ], command line [ %s ]",
                                 task->pid, task->comm );
       }
       return 0;
}

void traverse_exit(void) {

       printk( KERN_INFO "Module traverse unloaded\n");
}

module_init(traverse_init)
module_exit(traverse_exit)

MODULE_LICENSE("GPL");
