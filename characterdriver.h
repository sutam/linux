/*
 *
 *    characterdriver.h
 *
 *    Description:
 *           
 *         Prototype file for characterdriver.c
 *
 */
#include "ioctl.h"

#define SUCCESS  0
#define DEVICE_NAME	"chardev"	/* device name as appears in /proc/devices */
#define BUF_SZ	1024

/* 
 *  static global variables
 */
static int Major;               /* Major number assigned to our device driver */
static int Device_Open = 0;     /* Is device open?
                                 * Used to prevent multiple access to device */
static char* buffer;

/* prototypes */
int characterdriver_init(void);
void characterdriver_exit(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);
static long device_ioctl(struct file*, unsigned int cmd, unsigned long arg);

/* Defined in linux/fs.h, the file_operations structure holds
   pointers to functions defined by the driver that perform
   various operations on the device.

   struct file_operations {
	struct module *owner;
	 loff_t(*llseek) (struct file *, loff_t, int);
	 ssize_t(*read) (struct file *, char __user *, size_t, loff_t *);
	 ssize_t(*aio_read) (struct kiocb *, char __user *, size_t, loff_t);
	 ssize_t(*write) (struct file *, const char __user *, size_t, loff_t *);
	 ssize_t(*aio_write) (struct kiocb *, const char __user *, size_t,
			      loff_t);
	int (*readdir) (struct file *, void *, filldir_t);
	unsigned int (*poll) (struct file *, struct poll_table_struct *);
	int (*ioctl) (struct inode *, struct file *, unsigned int,
		      unsigned long);
	int (*mmap) (struct file *, struct vm_area_struct *);
	int (*open) (struct inode *, struct file *);
	int (*flush) (struct file *);
	int (*release) (struct inode *, struct file *);
	int (*fsync) (struct file *, struct dentry *, int datasync);
	int (*aio_fsync) (struct kiocb *, int datasync);
	int (*fasync) (int, struct file *, int);
	int (*lock) (struct file *, int, struct file_lock *);
	 ssize_t(*readv) (struct file *, const struct iovec *, unsigned long,
			  loff_t *);
	 ssize_t(*writev) (struct file *, const struct iovec *, unsigned long,
			   loff_t *);
	 ssize_t(*sendfile) (struct file *, loff_t *, size_t, read_actor_t,
			     void __user *);
	 ssize_t(*sendpage) (struct file *, struct page *, int, size_t,
			     loff_t *, int);
	unsigned long (*get_unmapped_area) (struct file *, unsigned long,
					    unsigned long, unsigned long,
					    unsigned long);
  };
*/

/* fop structures for character device */
static struct file_operations fops = {
        .read = device_read,
        .write = device_write,
        .open = device_open,
        .release = device_release,
	.unlocked_ioctl = device_ioctl
};

