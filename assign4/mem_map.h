
#define PROC_ENTRY_FILENAME	"mem_map"
#define MAX_PROC_SIZE		1024
#define SUCCESS			0

static struct proc_dir_entry * proc_entry;
static char proc_buffer[MAX_PROC_SIZE];
static unsigned long read_index = 0;
static int finished = 0;
static unsigned long num_physpages = 0;

int mem_map_init(void);
void mem_map_exit(void);
static int mem_map_open( struct inode *inode, struct file * file );
static int mem_map_close( struct inode * inode, struct file * file );
static ssize_t mem_map_read( struct file *filp, char *buffer, size_t length, loff_t *offset );

static struct file_operations mem_map_fops = {

       .owner = THIS_MODULE,
       .open  = mem_map_open,
       .read  = mem_map_read,
       .release = mem_map_close
};

