
#include <linux/ioctl.h>

#define MAJOR_NO 249

#define IOCTL_GET_MSG_LEN    	_IOR( MAJOR_NO, 1, int )
#define IOCTL_SET_MSG           _IOR( MAJOR_NO, 2, char*)
