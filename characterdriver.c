/*
 *   Homework Assignment 2
 *   
 *       CharacterDriver.c
 *       By Sui Tam
 *
 *   Description:
 *       
 *       A simple character device driver module that performs
 *       the basic open/read/write/ioctl/close operation
 *       This kernel module implements the basic character device driver functions
 *       Module should perform the operation in following scenario:
 *         
 *          * Init Module - Allocate a buffer of size 1024 bytes
 *          * Remove Module - Free the buffer you allocated in Init Module
 *          * Open - Initialize the buffer to a known text. say 'Welcome to UCSC second assignment'
 *          * Close - Initialize the buffer to empty string. Simply wipe out the contents
 *          * Read - Return the contents of the buffer, which will be printed from the user program
 *          * Write - User program provides the string to update the buffer and module has to overwrite the previous contents
 *          * unlocked_ioctl -
 *            (1) From user program read the size ( number of characters ) of the string
 *            (2) Reinitialize the buffer to 'Welcome to UCSC Second Assignment'
 *
 */

#include <linux/kernel.h>    /* printk()  */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>      /* kmalloc() */
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>     /* size_t    */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include "characterdriver.h"

int characterdriver_init(void) {

       Major = register_chrdev(0, DEVICE_NAME, &fops);

       if (Major < 0) {
          printk( "<1> KERN_ALERT:  Registering char device failed with %d\n", Major );
          return Major;
       }
 
       /* Allocate a buffer of size 1024 bytes */
       printk (KERN_INFO "<1>Init Module - Allocate a buffer of size %d bytes ...\n", BUF_SZ);
       buffer = kmalloc( BUF_SZ, GFP_KERNEL );
       if (buffer == NULL)
       {
           characterdriver_exit();
	   return -ENOMEM;
       }
       else
       	   printk (KERN_INFO "<1>Buffer of size %d bytes allocated\n", BUF_SZ);

       /* Initialize buffer to 0 */
       memset( buffer, 0, BUF_SZ); 

       printk(KERN_INFO "<1>I was assigned major number %d. To talk to\n", Major);
       printk(KERN_INFO "<1>the driver, create a dev file with\n");
       printk(KERN_INFO "<1>'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
       printk(KERN_INFO "<1>Try various minor numbers. Try to cat and echo to\n");
       printk(KERN_INFO "<1>the device file.\n");
       printk(KERN_INFO "<1>Remove the device file and module when done.\n");

       return SUCCESS;
}

void characterdriver_exit(void) {

       /* Unregister the device */
       unregister_chrdev(Major, DEVICE_NAME);

       printk (KERN_INFO "<1>Remove module - Free the buffer that was allocated in Init ...\n");
       if (buffer)
       {
           kfree( buffer );
       }
}

static int device_open(struct inode *inode, struct file *file)
{
       if (Device_Open)
           return -EBUSY;

       Device_Open++;

       printk ( KERN_INFO "<1>Open - Initialize the buffer to 'Welcome to UCSC second assignment'\n");
       sprintf( buffer, "Welcome to UCSC second assignment" );

       try_module_get(THIS_MODULE);

       return SUCCESS;
}

static int device_release( struct inode *inode, struct file * file )
{
       Device_Open--;
       printk( KERN_INFO "<1>Close - Initialize the buffer to empty string. Simply wipe out the contents\n");
       memset( buffer, 0, BUF_SZ );
       module_put( THIS_MODULE );

       return SUCCESS;
}

static ssize_t device_read( struct file* filePtr, char* userbuf, size_t count, loff_t *fpos )
{
        int bytes_read = 0;
	int result;

 	printk( KERN_INFO "<1>Read - Return the contents of the buffer, which will be printed from the user program\n");

        if (count <  strlen(buffer))
	{
            result = copy_to_user( userbuf, buffer, count);
	    bytes_read = count - result;
	}
	else
	{
	    result = copy_to_user( userbuf, buffer, strlen(buffer));
	    bytes_read = strlen(buffer) - result;
	}

	if ( *fpos == 0 )
        {
             /*
	      *fpos += bytes_read; 
              */
              return bytes_read;
	} 
        else
	{
	      return 0;
        }
}

static ssize_t device_write  (struct file * fptr, const char * userbuf, size_t length, loff_t *fpos)
{
	int result;

 	printk( KERN_INFO "<1>Write - User program provides the string to update the buffer and module has to overwrite the previous contents\n" );

	result = copy_from_user( buffer, userbuf, length );
	printk( KERN_INFO "After update, buffer is %s", buffer);

	return length-result;
}

static long device_ioctl( struct file *fptr, unsigned int ioctl_num, unsigned long ioctl_param )
{
	int i;
        char *temp;
        char  ch;
     
/*
 *          * unlocked_ioctl -
 *            (1) From user program read the size ( number of characters ) of the string
 *                ioctl_num: IOCTL_GET_MSG_LEN
 *
 *            (2) Reinitialize the buffer to 'Welcome to UCSC Second Assignment'
 *                ioctl_num: IOCTL_SET_MSG
 *
 */

        switch (ioctl_num) {
            case IOCTL_SET_MSG:
		temp =  (char * ) ioctl_param;

                get_user(ch, temp);
                for (i = 0; ch && i < BUF_SZ; i++, temp++ )
                     get_user( ch, temp);
                device_write( fptr, (char *) ioctl_param, i, 0 );
                break;

            case IOCTL_GET_MSG_LEN:
		return strlen(buffer);

 	    default:
                break;
	}
	return SUCCESS;
}	

module_init(characterdriver_init)
module_exit(characterdriver_exit)

MODULE_LICENSE("GPL");
