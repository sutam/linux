/*
 *    mem_map_flag.c
 *
 *        A kernel module to dump the mem_map flags field for each entry of the 
 *        mem_map array
 * 
 *        This is used to compare with the dump from the user program
 *        for assignment 5
 *
 */
#include <linux/kernel.h>    /* printk()  */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>      /* kmalloc() */
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>     /* size_t    */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/mm_types.h>
#include <linux/page-flags.h>
#include <asm/uaccess.h>
#include <asm/page.h>
#include "mem_map_flag.h"

int mem_map_init(void) {

       printk ( KERN_INFO "start loading /proc/%s\n", PROC_ENTRY_FILENAME );
       proc_entry = proc_create( "mem_map_flag", 0, NULL, &mem_map_fops );
       if ( proc_entry == NULL ) {
            printk( KERN_ALERT "Error: Cannot initialize /proc/mem_map_flag\n");
            return -ENOMEM;
       }
       printk( KERN_INFO "/proc/%s loaded\n", PROC_ENTRY_FILENAME );

       return SUCCESS;
}

void mem_map_exit(void) {

       struct proc_dir_entry *proc_root = NULL;

       remove_proc_entry( PROC_ENTRY_FILENAME, proc_root);
       printk( KERN_INFO "/proc/%s unloaded\n", PROC_ENTRY_FILENAME);
}

static int mem_map_open(struct inode *inode, struct file *file)
{
       int len;

       try_module_get(THIS_MODULE);
       num_physpages = get_num_physpages();
       len = sprintf( proc_buffer, "mem_map Virtual Address = %p Physical Address = %x Number of Pages = %lu Struct page size in bytes %d\n", 
                      pfn_to_page(0), virt_to_phys( pfn_to_page(0) ),
                      get_num_physpages(), sizeof(struct page) );
       printk ( KERN_INFO "%s", proc_buffer );

       return SUCCESS;
}

static int mem_map_close( struct inode *inode, struct file * file )
{
       module_put( THIS_MODULE );

       return SUCCESS;
}

static ssize_t mem_map_read( struct file* filePtr, char* userbuf, size_t count, loff_t *fpos )
{
       int len = 0;
       ssize_t bytes_read = 0;
       int result;
       struct page * page_ptr = NULL;
        
       if (finished ) {
           printk( KERN_INFO "mem_map_read: END\n");

           read_index = 0;
           finished = 0;
           num_reserved_pages = 0;
           return 0; 
       }

       page_ptr = pfn_to_page(read_index);
       if ( page_ptr == NULL )
       {
            printk( KERN_ALERT "Failed to retrieve page %lu\n", read_index );
            return -EFAULT;
       }
#ifdef SHOW_PAGE_FLAG
       len = sprintf( proc_buffer, "mem_map Page %lu Virtual Address = %p Physical Address = %x page->flags = %lu page reserved: %d\n",
                          read_index, pfn_to_page(read_index), virt_to_phys( pfn_to_page(read_index) ), page_ptr->flags, PageReserved( page_ptr ));
       
#else
       len = sprintf( proc_buffer, "mem_map Page %lu Virtual Address = %p Physical Address = %x page reserved: %d\n",
                          read_index, pfn_to_page(read_index), virt_to_phys( pfn_to_page(read_index) ), PageReserved( page_ptr ));
#endif

       if (PageReserved( page_ptr ))
       {
           num_reserved_pages++;
       }

       if (read_index + 1 == num_physpages )
       {
           len = sprintf( proc_buffer, "%s Number of reserved pages are %lu\n",
                                       proc_buffer, num_reserved_pages );
       }

       printk ( KERN_INFO "%s", proc_buffer );
       if ( count < strlen( proc_buffer ))
       {
            result = copy_to_user( userbuf, proc_buffer, count  ) ;
            bytes_read = count - result;
       } else {
            result = copy_to_user( userbuf, proc_buffer, strlen(proc_buffer) );
            bytes_read = strlen(proc_buffer) - result;
       }
       memset( proc_buffer, 0, MAX_PROC_SIZE );

       printk( KERN_INFO "mem_map_flag_read: read %ul bytes", bytes_read );

       read_index++;
       if (read_index == num_physpages)
       {
           finished = 1;
       }

       return bytes_read;
}

module_init(mem_map_init)
module_exit(mem_map_exit)

MODULE_LICENSE("GPL");
