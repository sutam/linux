/*
 *
 *   mem_map_flag_user.h
 *
 *      Description:  Header file for mem_map_flag_user.c
 *
 */
#define STRUCT_PAGE_SIZE                0x20                        /* size of struct page
                                                                       it can be verified
                                                                       from the kernel program
                                                                       that dump the 
                                                                       struct page size ( mem_map_flag_kernel_log ) */
#define PAGE_SIZE                       sysconf(_SC_PAGE_SIZE)      
#define NUM_STRUCT_PAGES_PER_PAGE       PAGE_SIZE / STRUCT_PAGE_SIZE
#define BUFSIZE                         1024
typedef unsigned long long              u64;

/* the entries in the address dump from assignment 4 will be as follows:

   page_number    virtual_address     physical_address    number_of_pages for mem_map( for the header only )

*/
typedef struct my_pageinfo {
       unsigned long  pagenum;                  // page_number input
       u64            virtual_addr;             // virtual_address input
       u64            physical_addr;            // physical_address input
} MY_PAGEINFO;

unsigned long mem_map_size;
MY_PAGEINFO pageinfo;

/* routines to mmap physical address from mem_map */
MY_PAGEINFO* init_page_info( MY_PAGEINFO* info );
int          get_mem_map_size( FILE* my_page_file, unsigned long* mem_map);
int          get_pages_addr( FILE* my_page_file, MY_PAGEINFO* pageinfo );
int          dev_mem_map_file( char* address_file );                         /* routine to map physical address from mem_map result from assignment 4 */
int          dev_mem_map( char* addr, char* mem_map_size );                  /* routine to map physical address starting from a starting address from a kernel program */

