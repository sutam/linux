/* *
 *  mem_map_mmap.c
 *
 *  Description:  Assignment 5 - Accessing physical memory locations from user space program
 *                Write a user program to access the physical memory using /dev/mem
 *                Please note that addresses that you need to use here must be physical addresses, not virtual.
 *                Specific tasks of this program:
 *                   - Read the physical address displayed in /proc/mem_map from previous assignment
 *                   - Now go through the entire struct page entries and see how many pages has flags bit PG_reserved bit set.
 *                
 *                May either use standard seek/read or mmap to achieve the result
 *                This source file uses the mmap method to read from physical address 
 *                in user space.
 *                In order to use mmap and bypass the priviledge protection:
 *                1)  One has to compile the kernel with the config option CONFIG_STRICT_DEVMEM as not set
 *                2)  Reinstall the kernel
                  3)  On x86 architectures with PAT(Page Attribute Table) support
                      there is also access restriction, so one has to modify the kernel
 *                    boot option to include the nopat argument to disable PAT
 * 
 *                Note:
 *                      Need to be root user in order to run this user program.
 *                      Write operation is discouraged because of system screw up in case of error
 *
 */
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include "page-flags.h" 
#include "mem_map_flag_user.h"

int main( int argc, char* argv[] )
{
    unsigned long addr;
    int    len;
    int    success;

    if ( argc == 1 )
    {
         printf( "mem_map_user [ <mem_map_file> | <starting_address> <mem_map_size>" );
	 return 0;
    }
    

    if (argc > 2 )
    {
         /*   
            dump virtual mapped address to physical address starting
            from the starting address of mem_map
          */
         dev_mem_map( argv[1], argv[2] );
    }
    else {
         /*
            dump virtual mapped address to physical address dumped from
            each mem_map location as obtained from assignment 4 
          */
         dev_mem_map_file( argv[1] );
    }
}

MY_PAGEINFO* init_page_info( MY_PAGEINFO* info )
{
       info->pagenum = -1;
       info->virtual_addr = -1;
       info->physical_addr = -1;
}

int get_mem_map_size( FILE* my_page_file, unsigned long* size)
{
       char line[BUFSIZE];
       unsigned long filler1, filler2;

       /* printf("get_mem_map_size ...\n"); */
       memset( line, 0, BUFSIZE);
       if (fgets( line, BUFSIZE, my_page_file ) != NULL )
       {
           sscanf( line, "mem_map Virtual Address = %x Physical Address = %x Number of Pages = %lu",
                          &filler1, &filler2, size );
           /*printf("mem_map_size %lu\n", *size );*/
	   return 1;
       } else
       {
           printf("End of file");
           return 0;
       }
       return 0;
}

int get_pages_addr( FILE* my_page_file, MY_PAGEINFO* pageinfo )
{
       char line[BUFSIZE];

       memset(line, 0, BUFSIZE);
       if (fgets( line, BUFSIZE, my_page_file ) != NULL )
       {
           sscanf( line, "mem_map Page %lu Virtual Address = %x Physical Address = %x", &pageinfo->pagenum, &pageinfo->virtual_addr, &pageinfo->physical_addr );
           /* 
               printf(" pagenum %lu  physical_addr %x \n",
               pageinfo->pagenum, pageinfo->physical_addr );
            */
	   return 1;
       } else
       {
           printf("End of file");
           return 1;
       }
       return 0;
}

/*
 *  mmap only works on offset address aligned to page boundary
 *  we need to find the last aligned page address, then 
 *  calculate the offset and adjust the memory offset 
 *  that we intend with this calculated offset
 */
void *align_mmap( int fd, u64 physical_addr, off_t *page_offset)
{
       void *result;

       /* calculate address of last page aligned to page boundary */
       off_t page_base = ( physical_addr / PAGE_SIZE ) * PAGE_SIZE;
       /* calculate the offset with our intended address          */
       *page_offset =  physical_addr - page_base;

       /* adjust the offset with the memory size we intend to retrieve,
          in this case it is the struct page size of 32 bytes */
       result = mmap( NULL, *page_offset + STRUCT_PAGE_SIZE, PROT_READ,
                       MAP_PRIVATE, fd, page_base);
       return result;
}

int dev_mem_map_file( char * address_file )
{
     int    i, isReserved, num_reserved, fd, success;
     void   * result;
     FILE   * my_page_file;
     off_t  offset;
     unsigned long * mem_map_flag_ptr;
     
     
     /* Open page file from assignment 4 */
     my_page_file = fopen( address_file, "r" );
     if (my_page_file == NULL)
     {
        printf("Cannot open mem_map file %s\n", address_file);
        return -1;
     }
    
     /* Open /dev/mem file descriptor */
     if ( (fd = open( "/dev/mem", O_RDWR|O_SYNC)) < 0 ) {
          printf ("Error opening /dev/mem.\n" );
          return -1;
     }

     /* Read the first line of the file */
     /* and get how many struct page entries are there in mem_map */
     success = get_mem_map_size( my_page_file, & mem_map_size );
     if (! success )
     {
        printf("Cannot read mem_map file %s\n", address_file);
        return -1;
     }

     /* 
      * We map the individual mem_map_entry from assignment 4 with the
      * physical address and read the struct page 
      */
     /*printf("Performing mmap on physical address obtained from assignment 4\n");*/
     num_reserved = 0;
     for ( i = 0; i < mem_map_size; i ++ )
     {
          success = get_pages_addr( my_page_file, init_page_info(& pageinfo ) );
          if ( !success )
          {
              printf("Cannot read mem_map file %s\n", address_file);
              return -1;
          }
          /* get the last aligned mapped virtual address from the physical address with mmap */
          result = align_mmap( fd, pageinfo.physical_addr, &offset);
          if (result == MAP_FAILED)
          {
              printf( "MMAP failed to map address of first struct page with system error %s\n", strerror(errno));
	      return -1;
          } else
          {
              char* mapped_virtual_address = ( char * ) result;
              /* The mem_map pointer virtual address is aligned address adjusted by offset */
              /* printf( "virtual address of mem_map_ptr is %x\n",  mapped_virtual_address + offset ); */
              /* Also, the flags field in page is the first unsigned long in the struct page structure 
                 defined in kernel source include/linux/mm_types.h 
                 struct page { 
                    // First double word block 
                       unsigned long flags;     // Atomic flags, some possibly
                                                // updated asynchronously 
                       ...
                 }
              */
              mem_map_flag_ptr = (unsigned long* ) (mapped_virtual_address + offset) ;
              isReserved = test_bit( PG_reserved, mem_map_flag_ptr );
              isReserved ? num_reserved++ : num_reserved ; 
	      printf( "mem_map Page %lu Virtual Address = %x ", i, pageinfo.virtual_addr );
              printf( "Physical Address = %x ", pageinfo.physical_addr);
#ifdef SHOW_PAGE_FLAG
              printf( "page->flags value = %lu page reserved: %d\n", *mem_map_flag_ptr, isReserved );
#endif
              printf( "page reserved: %d\n", isReserved );
              munmap( result, offset + STRUCT_PAGE_SIZE);
          }
     }
     printf( " Number of reserved pages are %d\n", num_reserved );

     fclose ( my_page_file );
     close( fd );
     return 0;
}

int dev_mem_map(char* address, char* size )
{
     int    fd, i, isReserved;
     void   * result;
     unsigned long    physical_addr, mem_map_size, isNumber;
     unsigned long    num_reserved;
     off_t  offset;
     unsigned long * mem_map_flag_ptr;
     
     if ( (fd = open( "/dev/mem", O_RDWR|O_SYNC)) < 0 ) {
          printf ("Error opening /dev/mem.\n" );
          return -1;
     }

     isNumber =  sscanf( address, "%x", &physical_addr ) != -1;
     if (!isNumber)
     {
          printf("Error: Illegal starting address\n");
          return -1;
     }
     isNumber = sscanf( size, "%lu", &mem_map_size ) != -1;
     if (!isNumber)
     {
          printf("Error: Illegal mem_map size\n");
          return -1;
     }
     printf("Performing mmap on physical address obtained from kernel module\n");
     num_reserved = 0;
     for ( i = 0; i < mem_map_size; i ++ )
     {
          /* get the mapped virtual address from the physical address with mmap */
          result = align_mmap( fd, physical_addr, &offset);
          if (result == MAP_FAILED)
          {
              printf( "MMAP failed to map address of first struct page with system error %s\n", strerror(errno));
	      return -1;
          } else
          {
              char* mapped_virtual_address = ( char * ) result;
              /* printf( "virtual address of mem_map_ptr is %x\n",  mapped_virtual_address + offset ); */
              /* the flags field is the first unsigned long in the struct page structure 
                 defined in kernel source include/linux/mm_types.h 
                 struct page { 
                    // First double word block 
                       unsigned long flags;     // Atomic flags, some possibly
                                                // updated asynchronously 
                       ...
                 }
              */
              mem_map_flag_ptr = (unsigned long* ) (mapped_virtual_address + offset) ;
              isReserved = test_bit( PG_reserved, mem_map_flag_ptr );
              isReserved ? num_reserved++ : num_reserved ; 
	      printf( "mem_map Page %lu Physical Address = %x ", i, physical_addr);
              printf( "page reserved: %d\n", isReserved );
              munmap( result, offset + STRUCT_PAGE_SIZE);
          }
          /* increment to the next mem_map physical address */
          physical_addr += STRUCT_PAGE_SIZE;
     }
     printf( " Number of reserved pages are %d\n", num_reserved );
     return 0;
}
