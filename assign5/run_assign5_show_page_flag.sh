
echo "***Compile kernel module mem_map_flag to dump page flags field"
rm -f Makefile
cp Makefile_show_page_flag Makefile
make clean
make
echo
echo "***Install module mem_map_flag ... "
rmmod mem_map_flag
insmod mem_map_flag.ko
echo "***Finished installing module mem_map_flag"
echo
echo "***Compile user program to dump mem_map and page flags field"
rm -f mem_map_flag_user
gcc mem_map_flag_user.c -D SHOW_PAGE_FLAG -o mem_map_flag_user
echo "***Finished compiling user program"
echo
echo "***Running kernel module mem_map_flag to generate mmap_flag_kernel_log "
cat /proc/mem_map_flag > mmap_flag_kernel_page_flag_log
echo
echo "***Running user program mem_map_flag_user to generate mem_map_flag_user_log"
./mem_map_flag_user mem_map_dump_log > mmap_flag_user_page_flag_log
echo "***Showing all page flag logs "
ls *page_flag_log
echo
