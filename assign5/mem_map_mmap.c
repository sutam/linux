/*
 *
 *  mem_map_mmap.c
 *
 *  Description:  Assignment 5 - Accessing physical memory locations from user space program
 *                Write a user program to access the physical memory using /dev/mem
 *                Please note that addresses that you need to use here must be physical addresses, not virtual.
 *                Specific tasks of this program:
 *                   - Read the physical address displayed in /proc/mem_map from previous assignment
 *                   - Now go through the entire struct page entries and see how many pages has flags bit PG_reserved bit set.
 *                
 *                May either use standard seek/read or mmap to achieve the result
 *                This particular source file uses the mmap method to access physical
*                 memory from user space
 *                Note:
 *                      Need to be root user in order to run this user program.
 *                      Write operation is discouraged because of system screw up in case of error
 *
 */
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int main( int argc, char argv[] )
{
    void * result;
    int    fd;
    unsigned long physical_addr;
    int * addr;
    int    len;
    int    i;

    physical_addr =  0x36bfd000;
    len = sysconf(_SC_PAGE_SIZE);
    if ( (fd = open( "/dev/mem", O_RDWR|O_SYNC)) < 0 )
    {
          printf ("Error opening /dev/mem.\n" );
          return -1;
    }

    /* Using mmap, try getting address of first struct page */
    	result = mmap( NULL, 4096, PROT_READ,
                 MAP_PRIVATE, fd, physical_addr);
    	if (result == MAP_FAILED)
    	{
          	printf( "MMAP failed with system error %s\n", strerror(errno));
	  	return -1;
    	}  else
    	{
          	addr = (int *) result;
    		for ( i = 0; i < 1000; i++)
    		{
                	printf ( "index %d virtual address:  %p ", i, addr );
                	printf ( "inside first 4 bytes of virtual address: %d\n", *addr );
			addr += 0x8;
                }
	}
}
