echo "***Removing all log files from previous run "
rm -f *log
echo "***Finished removing old logs"
echo "***Preparing mem_map dump using assignment 4 "
pushd assign4
make clean
make
rmmod mem_map
insmod mem_map.ko
cat /proc/mem_map > ../mem_map_dump_log
echo "***Finished preparing mem_map"
popd
echo "***Compile kernel module mem_map_flag to dump mem_map and corresponding reserved flags field"
rm -f Makefile
cp Makefile_no_page_flag Makefile
make clean
make
echo
echo "***Install module mem_map_flag ... "
rmmod mem_map_flag
insmod mem_map_flag.ko
echo "***Finished installing module mem_map_flag"
echo
echo "***Compile user program to dump mem_map and corresponding flags field"
rm -f mem_map_flag_user
gcc mem_map_flag_user.c -o mem_map_flag_user
echo "***Finished compiling user program"
echo
echo "***Running kernel module mem_map_flag to generate mmap_flag_kernel_log "
cat /proc/mem_map_flag > mmap_flag_kernel_log
echo
echo "***Running user program mem_map_flag_user to generate mem_map_flag_user_log"
./mem_map_flag_user mem_map_dump_log > mmap_flag_user_log
echo
echo "***Comparing kernel and user program results"
diff mmap_flag_kernel_log mmap_flag_user_log 
echo "***Finished comparing kernel and user program"
