/*
 *  Homework Assignment 2
 *
 *       userprogram.c
 *       By Sui Tam
 *
 *   Description:
 *
 *       A user program that uses the character device driver module
 *       written in CharacterDriver.c
 *
 *       User will perform:
 *
 *          * Open the module file descriptor
 *          * ioctl - Find the size of default string
 *          * Read the string and print it
 *          * Write a new string 'String updated'
 *          * Read the string and print it again
 *          * ioctl - Find the size of string
 *          * ioctl - Reinitialize the buffer
 *          * Read the string and print it again
 *          * Close the module file descriptor
 */
#include "ioctl.h"
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>

#define DEVICE_NAME 	"/dev/chardev"
#define DEFAULT_STRING  "Welcome to UCSC Second Assignment"
#define BUF_SZ		1024

int ioctl_set_msg( int fd, char* msg ); 
int ioctl_get_msg_len( int fd, int*len );
int read_device( int deviceFd, char* msg, int msgsize, int msglen );
int write_device( int deviceFd, char* msg );

int main(void)
{

    int deviceFd;
    int returncode;
    int bytesRead;
    int bytesWrite;
    int msglen;
    char buffer[BUF_SZ];

    msglen = strlen(DEFAULT_STRING);
    /* Open the module file descriptor */
    printf("/* Open the module file descriptor */\n");
    deviceFd = open ( DEVICE_NAME, O_RDWR );
    if (deviceFd < 0 ) {
        printf ( "Failed to open %s\n", DEVICE_NAME );
	return -1 ;
    }

    /* ioctl - Find the size of default string */
    printf("/* Find the size of default string through ioctl 1 */\n");
    returncode = ioctl_get_msg_len( deviceFd, &msglen);
    if ( returncode == -1 )
    {
         printf("ioctl_get_msg_len failed\n");
	 return -1 ;
    } 

    /* Read the string and print it */
    printf ("/* Read the string and print it */\n");
    bytesRead = read_device( deviceFd, buffer, BUF_SZ, msglen );
    printf("Content of buffer is %s\n", buffer );

    /* Write a new string 'String updated' */
    printf("/* Write a new string 'String updated' */\n");
    bytesWrite = write_device( deviceFd, "String updated" );
    
    /* Read the string and print it again */
    printf("/* Read the string and print it again */\n");
    bytesRead = read_device( deviceFd, buffer, BUF_SZ, msglen );
    printf ("Content of buffer after updating is %s\n", buffer );
    

    /* ioctl - Find the size of string    */
    printf("/* Find the size of string through ioctl */\n");
    returncode = ioctl_get_msg_len( deviceFd, &msglen);
    if ( returncode == -1 )
    {
         printf("ioctl_get_msg_len failed\n");
         return -1 ;
    }
    printf( "Size of updated string is %d\n", msglen );
   
    /* ioctl - Reinitialize the buffer */
    printf("/* Reinitialize the buffer through ioctl 2 */\n");
    returncode = ioctl_set_msg( deviceFd, DEFAULT_STRING );

    /* Read the string and print it again */
    printf("/* Read the string and print it again */\n");
    bytesRead = read_device( deviceFd, buffer, BUF_SZ, msglen );
    printf ("Content of buffer after reinitializing is %s\n", buffer );

    /* Close the module file descriptor */
    if ( close( deviceFd ) == -1 )
    {
         printf("File descriptor unsuccessful\n");
	 return -1 ;
    }

    return 0;
}

int ioctl_set_msg( int fd, char* msg )
{
    int returncode;

    returncode = ioctl( fd, IOCTL_SET_MSG, msg );

    if ( returncode < 0 ) {
         printf( "ioctl_set_msg failed: %d\n", returncode );
         return -1;
    }
    return 0;
}
 
int ioctl_get_msg_len( int fd, int* len )
{
    *len = ioctl( fd, IOCTL_GET_MSG_LEN );
    return 0;
}

int read_device( int deviceFd, char* msg, int msgsize, int msglen )
{
    int bytesRead;

    memset( msg, 0, msgsize );
    bytesRead = read(  deviceFd, msg, msglen );
    if ( bytesRead == 0 )
    {
         printf("End of file reached \n");
    }
    if ( bytesRead == -1 )
    {
         printf("Error found in read_device()");
    }
    return bytesRead;
}

int write_device( int deviceFd, char* msg )
{
    int bytesWrite;

    bytesWrite = write( deviceFd, msg, strlen(msg));
    if ( bytesWrite == 0 )
    {
         printf("Error found in write_device()");
    }
    return bytesWrite;
}
